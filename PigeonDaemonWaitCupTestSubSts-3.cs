using System;
using TcpClient = NetCoreServer.TcpClient;
using System.IO.Ports;
using System.Threading;
using System.Net.Sockets;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Linq;
using Newtonsoft.Json.Linq;

using MySql.Data;
using MySql.Data.MySqlClient;

// 20200813 version begin 
using log4net;
using log4net.Config;
using System.Reflection;
// 20200813 version begin
namespace PigeonDaemon6214
{
    class ChatClient : TcpClient
    {
        public PigeonCtrl pigeonCtrl;
        public SerialPort pigeonPort;
        //public SerialPort sensorPort;
        public String lastCmd = "";
        //public ChatClient(string address, int port, SerialPort pigeonPort, SerialPort sensorPort) : base(address, port) {
        public ChatClient(string address, int port, SerialPort pigeonPort) : base(address, port)
        {
            this.pigeonPort = pigeonPort;
            //this.sensorPort = sensorPort;
        }

        public void DisconnectAndStop()
        {
            _stop = true;
            DisconnectAsync();
            while (IsConnected)
                Thread.Yield();
        }

        protected override void OnConnected()
        {
            Console.WriteLine($"Chat TCP client connected a new session with Id {Id}");
        }

        protected override void OnDisconnected()
        {
            Console.WriteLine($"Chat TCP client disconnected a session with Id {Id}");

            // Wait for a while...
            Thread.Sleep(1000);

            // Try to connect again
            if (!_stop)
                ConnectAsync();
        }

        protected override void OnReceived(byte[] buffer, long offset, long size)
        {
            int size32 = (int)size;
            int offset32 = (int)offset;
            //Console.WriteLine("Received : " + Encoding.UTF8.GetString(buffer, offset32, size32));

            var serverCmd = System.Text.Encoding.ASCII.GetString(buffer, 0, size32);
            Console.WriteLine("Received from server: " + serverCmd);

            ExecuteCommand(serverCmd);
            /*
            String cmdHead = serverCmd.Substring(0, 1);
            int pigeonNo = 0;
            try
            {
                pigeonNo = int.Parse(serverCmd.Substring(1, 1));
            }
            catch { }
            if (pigeonNo == 0)
            {
                Console.WriteLine("Warning command : " + serverCmd);
                return;
            }

            lastCmd = cmdHead;
            // H = pigeon door open, U = Up the pigeon door, S = Stop the pigeon door
            if (cmdHead == "H")
            {
                this.pigeonPort.WriteLine(serverCmd);
                // If the ardiuno program use Serial.readStringUntil(10) to read data send from here
                // then we don't need to add "Thread.Sleep(1000)", otherwise if the time of two commands come very close
                // then two commands will become one and make the second command cannot be run 

                //pigeonCtrls[pigeonNo - 1].SetTimerToStop();
                pigeonCtrl.SetTimerToStop();
            }
            else if (cmdHead == "U")
            {
                this.pigeonPort.WriteLine(serverCmd);
                //pigeonCtrls[pigeonNo - 1].SetTimersToCloseDoor();
                pigeonCtrl.SetTimersToCloseDoor();
            }
            */

        }

        protected override void OnError(SocketError error)
        {
            Console.WriteLine($"Chat TCP client caught an error with code {error}");
        }

        private bool _stop;

        public void PigeonDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var pigeonReply = pigeonPort.ReadLine();
            Console.WriteLine($"Pigeon Received : {pigeonReply}");
        }

        public void SensorDataReceived(object sender, SerialDataReceivedEventArgs e)

        {
            //var sensorReply = sensorPort.ReadLine();
            //var sensorReplyHead = sensorReply.Substring(0, 1);
            //var sensorReplyNo = sensorReply.Substring(1, 1);
            // sensorReply : Y=have object here, N=No object here
            //Console.WriteLine("Sensor Received : " + sensorReply);
            //Console.WriteLine("Last Cmd : " + lastCmd);

            // pass back up signal "S" to pigeonPort if last moment was down "H"
            //if (lastCmd == "H" && sensorReplyHead == "S")
            // H come from tcp

            // just test ...
            //if (lastCmd == "H" && sensorReplyHead == "N") {
            //    pigeonPort.WriteLine("X"+ sensorReplyNo);
            //    lastCmd = "X";
            //}

            /*
            if (lastCmd == "H" && sensorReplyHead == "N") {
                //pigeonPort.WriteLine(sensorReply);
                // close door
                pigeonPort.WriteLine("S5");
                //Thread.Sleep(500);
                lastCmd = "S";
            } else if (lastCmd == "S" && sensorReplyHead == "N") {
                // stop motor
                pigeonPort.WriteLine("X5");
                lastCmd = "X";
            }
            */
        }
        public void ExecuteCommand(string command)
        {
            Console.WriteLine($"{command} executed !");
            if (this.lastCmd == command)
            {
                return;
            }
            this.lastCmd = command;
            String cmdHead = command.Substring(0, 1);
            int pigeonNo = 0;
            try
            {
                pigeonNo = int.Parse(command.Substring(1, 1));
            }
            catch { }
            if (pigeonNo == 0)
            {
                Console.WriteLine("Warning command : " + command);
                return;
            }

            // H = pigeon door open, U = Up the pigeon door, S = Stop the pigeon door
            if (cmdHead == "H")
            {
                this.pigeonPort.WriteLine(command);
                // If the ardiuno program use Serial.readStringUntil(10) to read data send from here
                // then we don't need to add "Thread.Sleep(1000)", otherwise if the time of two commands come very close
                // then two commands will become one and make the second command cannot be run 

                //pigeonCtrls[pigeonNo - 1].SetTimerToStop();

                // version-1
                pigeonCtrl.SetTimerToOpenDoor();
                // version-1.1
                //Task.Delay(TimeSpan.FromMilliseconds(1800)).ContinueWith(task => this.pigeonCtrl.SendPigeonStopThenWait(task));

            }
            else if (cmdHead == "U")
            {
                this.pigeonPort.WriteLine(command);
                //pigeonCtrls[pigeonNo - 1].SetTimersToCloseDoor();

                // version-1
                pigeonCtrl.SetTimersToCloseDoor();
                // version-1.1
                //Task.Delay(TimeSpan.FromMilliseconds(1500)).ContinueWith(task => this.pigeonCtrl.SendPigeonFixing(task));
                //Task.Delay(TimeSpan.FromMilliseconds(2600)).ContinueWith(task => this.pigeonCtrl.SendPigeonStop(task));

            }

        }

    }

    class PigeonCtrl
    {
        public SerialPort pigeonPort;
        public int pigeonNo;
        public Timer openStopTimer;
        public Timer closeStopTimer;
        public Timer closingTimer;
        public Timer fixingTimer;
        public int sensorState;
        public ChatClient chatClient;
        public PigeonCtrl(int No, SerialPort pigeonPort)
        {
            this.pigeonNo = No;
            this.pigeonPort = pigeonPort;
            var autoEvent = new AutoResetEvent(false);
            openStopTimer = new Timer(SendPigeonStopThenWait, autoEvent, Timeout.Infinite, Timeout.Infinite);
            closeStopTimer = new Timer(SendPigeonStop, autoEvent, Timeout.Infinite, Timeout.Infinite);
            closingTimer = new Timer(SendPigeonClosing, autoEvent, Timeout.Infinite, Timeout.Infinite);
            fixingTimer = new Timer(SendPigeonFixing, autoEvent, Timeout.Infinite, Timeout.Infinite);
        }

        public void TimerDebug(Object stateInfo)
        {
            Console.WriteLine($"TimerDebug !");
        }

        public void SendPigeonStop(Object stateInfo)
        {
            this.pigeonPort.WriteLine("S" + pigeonNo.ToString());
            Console.WriteLine($"Pigeno {this.pigeonNo} S send !");
            chatClient.SendAsync("H" + pigeonNo.ToString() + "DONE");
            Console.WriteLine($"H{this.pigeonNo}DONE send !");
            // chatClient.lastCmd = "";
        }

        public void SendPigeonStopThenWait(Object stateInfo)
        {
            this.pigeonPort.WriteLine("S" + pigeonNo.ToString());
            Console.WriteLine($"Pigeno {this.pigeonNo} S send !");
            Thread.Sleep(1500);
            // wait for customer to pickup the drink
            do
            {
                Thread.Sleep(10);
                //if (sensorState == 0)

                // use timer to close door
                Thread.Sleep(3000);
                if (true)
                {
                    Thread.Sleep(2000);
                    chatClient.ExecuteCommand("U" + pigeonNo.ToString());
                    chatClient.lastCmd = "";

                    // new program with tcp
                    // Program.UpdateOrderStatus(o._id, "COLLECTED", "newprog");

                    break;
                }
            } while (true);
        }
        // fast closing the door around 2 seconds and shift to slow fixing the door
        public void SendPigeonClosing(Object stateInfo)
        {
            this.pigeonPort.WriteLine("U" + pigeonNo.ToString());
            Console.WriteLine($"Pigeno {this.pigeonNo} U send !");
        }
        // slower the motor speed to let the sensors to sences the exactly close position
        public void SendPigeonFixing(Object stateInfo)
        {
            this.pigeonPort.WriteLine("V" + pigeonNo.ToString());

            // while the door not exactly closed, try close it until closed
            //while (sensorState == 1)
            //{
            //    SetTimersToCloseDoor();
            //}

            Console.WriteLine($"Pigeno {this.pigeonNo} V send !");
        }
        public void SetTimerToOpenDoor()
        {
            //openStopTimer.Change(1800, 0);
            // for testing
            openStopTimer.Change(1300, 0);

            //openStopTimer.Change(1030, 0);
        }
        public void SetTimersToCloseDoor()
        {
            //fixingTimer.Change(1500, 0);  // U->V
            //closeStopTimer.Change(2600, 0);   // S
            // for testing
            fixingTimer.Change(900, 0);  // U->V
            closeStopTimer.Change(1400, 0);   // S


            //fixingTimer.Change(700, 0);  // U->V
            //closeStopTimer.Change(1800, 0);   // S
        }
    }

    class Program
    {

        // Create a new TCP chat client
        static List<ChatClient> clients = new List<ChatClient>();

        static List<PigeonCtrl> pigeonCtrls = new List<PigeonCtrl>();

        static PigeonReply pr = new PigeonReply();

        static SerialPort pigeonPort = null;
        public static HttpClient Client = new HttpClient();
        private static Credentials couchDbCredentials;
        private static readonly string AuthCouchDbCookieKeyName = "AuthSession";
        private static string dbName = "order_item";


        private static readonly string BaseCouchDbApiAddress = "http://localhost:5984";

        public static List<int> motorAddresses = null;
        // 20200813 version begin
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // 20200813 version end
        static void Main(string[] args)
        {
            // 20200813 version begin
            // Load configuration
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            log.Info("PigeonDaemon6214 Started!");
            // 20200813 version end

            // The appsettings.json files “Copy to Output Directory” property 
            // should also be set to “Copy if newer” so that the application is able to access it
            // when published.
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            String pigeonCom = configuration.GetConnectionString("PigeonCom");
            //String sensorCom = configuration.GetConnectionString("SensorCom");
            String pigeonCnt = configuration.GetConnectionString("PigeonCnt");

            //Console.WriteLine("PigeonCom : " + pigeonCom);
            //Console.WriteLine("PigeonCnt : " + pigeonCnt);
            log.Info("PigeonCom : " + pigeonCom);
            log.Info("PigeonCnt : " + pigeonCnt);

            /*
            // TCP server address
            //string address = "192.168.10.11";
            string address = "192.168.0.175";
            if (args.Length > 0)
                address = args[0];

            // TCP server port
            int port = 9001;
            if (args.Length > 1)
                port = int.Parse(args[1]);

            Console.WriteLine($"TCP server address: {address}");
            Console.WriteLine($"TCP server port: {port}");

            Console.WriteLine();
            */

            // Connect to Pigeon hole com port
            //SerialPort pigeonPort = new SerialPort(pigeonCom);
            pigeonPort = new SerialPort(pigeonCom);
            pigeonPort.BaudRate = 9600;
            //pigeonPort.BaudRate = 38400;
            pigeonPort.ErrorReceived += PigeonPort_ErrorReceived;
            pigeonPort.DataReceived += new SerialDataReceivedEventHandler(Pigeon_DataReceived);
            pigeonPort.Open();

            /*
            var pigeonCnt_int = int.Parse(pigeonCnt);
            for (var i = 0; i < pigeonCnt_int; i++)
            {
                // use pigeonCtrls to replace chatClient
                pigeonCtrls.Add(new PigeonCtrl(i + 1, pigeonPort));

            }

            //Console.WriteLine($"Listen sensor port : {sensorCom}");
            //sensorPort.DataReceived += new SerialDataReceivedEventHandler(client.SensorDataReceived);
            //pigeonPort.DataReceived += new SerialDataReceivedEventHandler(client.PigeonDataReceived);

            pigeonPort.DataReceived += new SerialDataReceivedEventHandler(PigeonDataReceived);
            */
            //string connStr = "server=127.0.0.1;user=root;database=crowndb;port=3306;password=crown@12345";
            // PS Database
            string connStr = "server=192.168.10.11;user=crown;database=crowndb;port=3306;password=busca";
            //string connStr = "server=localhost;user=crown;database=crowndb;port=3306;password=busca";
            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlConnection conn2 = new MySqlConnection(connStr);
            //Console.WriteLine("Connecting to MySQL...");
            log.Info("Connecting to MySQL...");
            //conn.Open();
            //conn2.Open();
            //Console.WriteLine("Connected to MySQL...");
            log.Info("Connected to MySQL...");
            string selectSql = null;
            selectSql += "select devicename, actuation_status from devicestate ";
            //selectSql += " where devicetype = 'PigeonHole' and deviceavailability = 'X' ";
            selectSql += " where devicetype = 'PigeonHole' ";
            //selectSql += " and actuation_status in ( 'DELIVERY', 'OPENING', 'START-OPEN' ) order by devicename";
            // 2020-09-03, google tumbler demo for just ust P3 for tumbler : start
            selectSql += " and ( actuation_status in ( 'TEST','TESTING', 'DELIVERY', 'OPENING', 'START-OPEN', 'TUMBLER', 'START-TUMB', 'OPEN-TUMB', 'HOME', 'HOMING' ) or actuation_status like 'OPENING%' ) order by devicename";
            // 2020-09-03, google tumbler demo for just ust P3 for tumbler : end

            string updateSql = null;
            updateSql += "update devicestate set actuation_status = @status, devicedownreason = @devicedownreason ";
            updateSql += " where devicename = @devicename and devicetype = 'PigeonHole' ";
            //updateSql +=   " and deviceavailability = 'X' ";

            MySqlCommand readCmd = new MySqlCommand(selectSql, conn);
            //readCmd.Prepare();

            MySqlCommand updateCmd = new MySqlCommand(updateSql, conn2);
            //updateCmd.Parameters.AddWithValue("@devicename", "");
            //updateCmd.Parameters.AddWithValue("@status", "");
            //updateCmd.Prepare();
            MySqlDataReader rdr = null;

            int pigeonNo = 0;
            byte[] bytesToSend;
            // translate the no. of pigeon hole to address
            motorAddresses = configuration.GetConnectionString("MotorAddresses").Split(',').Select(int.Parse).ToList();
            int motorAddress = 0;
            string devicename = null;
            string actuation_status = null;

            // sub-sts
            int v = Convert.ToInt32(pigeonCnt);
            DateTime[] startOpening = new DateTime[v];



            //PigeonReply pr = new PigeonReply();
            var reconnect = true;
            do
            {
                try
                {

                    // 20210310: new connection and reconnect when it is closed
                    //if (conn.State != System.Data.ConnectionState.Open)
                    if (reconnect)
                    {
                        conn = new MySqlConnection(connStr);
                        conn.Open();
                        readCmd = new MySqlCommand(selectSql, conn);
                        readCmd.Prepare();

                        conn2 = new MySqlConnection(connStr);
                        conn2.Open();
                        updateCmd = new MySqlCommand(updateSql, conn2);
                        updateCmd.Parameters.AddWithValue("@devicename", "");
                        updateCmd.Parameters.AddWithValue("@status", "");
                        updateCmd.Parameters.AddWithValue("@devicedownreason", "");
                        updateCmd.Prepare();
                        reconnect = false;
                    }
                    rdr = readCmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        // 20210205: reconnect connection when it is closed
                        devicename = rdr.GetString("devicename");
                        actuation_status = rdr.GetString("actuation_status");
                        pigeonNo = int.Parse(devicename.Substring(1, 1));
                        motorAddress = GetAddressByPigeon(pigeonNo);
                        //Console.WriteLine($"{devicename}, {actuation_status}");
                        log.Info($"{devicename}, {actuation_status}");

                        if (actuation_status == "DELIVERY" || actuation_status == "START-OPEN")
                        {
                            pr = null;
                            //bytesToSend = new byte[8] { Convert.ToByte(motorAddress), 0x09, 0x00, 0x02, 0x00, 0x00, 0x00, 0x01 };
                            // SGP : for Trinamic TMCM-6214, just have one card, so the first address is 0x00 
                            bytesToSend = new byte[8] { 0x01, 0x09, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x01 };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                            updateCmd.Parameters["@status"].Value = "START-OPEN";
                            updateCmd.ExecuteNonQuery();

                            //Thread.Sleep(100);
                            /*
                            Thread.Sleep(500);
                            pr = GetReply();
                            */
                            //if (pr.commandNo == 9 && pr.status == 100 && pr.value4 == 1)
                            // just one more try
                            if (WaitReplyUntilTimeout())
                            {
                                //if ((pr.commandNo == 9 && pr.status == 100 && pr.value4 == 1) || actuation_status == "START-OPEN")
                                if (pr.commandNo == 9 && pr.status == 100 && pr.value4 == 1)
                                {
                                    //updateCmd.Parameters["@devicename"].Value = "S" + pr.replyPigeonNo;
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "OPENING";
                                    updateCmd.ExecuteNonQuery();
                                    startOpening[pigeonNo - 1] = DateTime.Now;
                                } // set global user variable = 1 to open the door
                            }

                        }
                        else if (actuation_status.Substring(0, 7) == "OPENING" || actuation_status.Substring(0, 6) == "HOMING")
                        {
                            pr = null;
                            //bytesToSend = new byte[8] { Convert.ToByte(motorAddress), 0x0A, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00 };
                            // GGP : for Trinamic TMCM-6214, just have one card, so the first address is 0x00 
                            bytesToSend = new byte[8] { 0x01, 0x0A, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x00 };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            //Thread.Sleep(100);
                            /*
                            Thread.Sleep(500);
                            pr = GetReply();
                            */
                            //if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 4)
                            // for 6214, the final status is 9
                            if (WaitReplyUntilTimeout())
                            {
                                if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 9)
                                {
                                    pr = null;
                                    // wait cup global parameter value start from 130
                                    bytesToSend = new byte[8] { 0x01, 0x0A, Convert.ToByte(130 + motorAddress), 0x02, 0x00, 0x00, 0x00, 0x00 };
                                    bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                                    pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                                    pigeonPort.BaseStream.Flush();

                                    /*
                                    Thread.Sleep(500);
                                    pr = GetReply();
                                    */
                                    if (WaitReplyUntilTimeout())
                                    {
                                        if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 1)
                                        {
                                            updateCmd.Parameters["@devicedownreason"].Value = "WAIT-CUP";
                                        }
                                        else
                                        {
                                            updateCmd.Parameters["@devicedownreason"].Value = null;
                                        }
                                        //updateCmd.Parameters["@devicename"].Value = "S" + pr.replyPigeonNo;
                                        updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                        updateCmd.Parameters["@status"].Value = "RELEASE";
                                        // for infinity loop testing
                                        //updateCmd.Parameters["@status"].Value = "DELIVERY";
                                        updateCmd.ExecuteNonQuery();
                                    }
                                }
                                else if (actuation_status.Substring(0, 7) == "OPENING" && (DateTime.Now - startOpening[pigeonNo - 1]).TotalSeconds >= 3 && (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 1))
                                {
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "DELIVERY";
                                    updateCmd.ExecuteNonQuery();
                                    startOpening[pigeonNo - 1] = DateTime.Now;
                                }
                                else if (actuation_status.Substring(0, 6) == "HOMING" && (DateTime.Now - startOpening[pigeonNo - 1]).TotalSeconds >= 3 && (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 3))
                                {
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "HOME";
                                    updateCmd.ExecuteNonQuery();
                                    startOpening[pigeonNo - 1] = DateTime.Now;
                                }
                                else
                                {
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = ((actuation_status.IndexOf("-") == -1) ? actuation_status : actuation_status.Substring(0,actuation_status.IndexOf("-"))) + "-" + pr.value4.ToString();
                                    updateCmd.ExecuteNonQuery();
                                }
                            }
                        }
                        // 2020-09-14 : homing
                        else if (actuation_status == "HOME" || actuation_status == "START-HOME")
                        {
                            pr = null;
                            //bytesToSend = new byte[8] { Convert.ToByte(motorAddress), 0x09, 0x00, 0x02, 0x00, 0x00, 0x00, 0x01 };
                            // SGP : for Trinamic TMCM-6214, just have one card, so the first address is 0x00 
                            bytesToSend = new byte[8] { 0x01, 0x09, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x03 };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                            updateCmd.Parameters["@status"].Value = "START-HOME";
                            updateCmd.ExecuteNonQuery();

                            //Thread.Sleep(100);
                            /*
                            Thread.Sleep(500);
                            pr = GetReply();
                            */
                            if (WaitReplyUntilTimeout())
                            {
                                if (pr.commandNo == 9 && pr.status == 100 && pr.value4 == 3)
                                {
                                    //updateCmd.Parameters["@devicename"].Value = "S" + pr.replyPigeonNo;
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "HOMING";
                                    updateCmd.ExecuteNonQuery();
                                    startOpening[pigeonNo - 1] = DateTime.Now;
                                } // set global user variable = 1 to open the door
                            }
                        }
                        else if (actuation_status == "TEST" || actuation_status == "START-TEST")
                        {
                            pr = null;
                            bytesToSend = new byte[8] { 0x01, 0x09, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x0E };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                            updateCmd.Parameters["@status"].Value = "START-TEST";
                            updateCmd.ExecuteNonQuery();

                            if (WaitReplyUntilTimeout())
                            {
                                //if ((pr.commandNo == 9 && pr.status == 100 && pr.value4 == 14) || actuation_status == "START-TEST")
                                if (pr.commandNo == 9 && pr.status == 100 && pr.value4 == 14)
                                {
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "TESTING";
                                    updateCmd.ExecuteNonQuery();
                                } // set global user variable = 1 to open the door
                            }
                        }
                        else if (actuation_status == "TESTING")
                        {
                            pr = null;
                            bytesToSend = new byte[8] { 0x01, 0x0A, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x00 };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            if (WaitReplyUntilTimeout())
                            {
                                if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 9)
                                {
                                    updateCmd.Parameters["@devicedownreason"].Value = null;
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "RELEASE";
                                    updateCmd.ExecuteNonQuery();
                                }
                                else
                                {
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = actuation_status + "-" + pr.value4.ToString();
                                    updateCmd.ExecuteNonQuery();
                                }
                            }
                        }
                        // 2020-09-03, google tumbler demo for just ust P3 for tumbler : start
                        else if (pigeonNo == 3 && (actuation_status == "TUMBLER" || actuation_status == "START-TUMB"))
                        {
                            pr = null;
                            //bytesToSend = new byte[8] { Convert.ToByte(motorAddress), 0x09, 0x00, 0x02, 0x00, 0x00, 0x00, 0x01 };
                            // SGP : for Trinamic TMCM-6214, just have one card, so the first address is 0x00 
                            bytesToSend = new byte[8] { 0x01, 0x09, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x0A };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                            updateCmd.Parameters["@status"].Value = "START-TUMB";
                            updateCmd.ExecuteNonQuery();

                            //Thread.Sleep(100);
                            /*
                            Thread.Sleep(500);
                            pr = GetReply();
                            */
                            if (WaitReplyUntilTimeout())
                            {
                                if (pr.commandNo == 9 && pr.status == 100 && pr.value4 == 10)
                                {
                                    //updateCmd.Parameters["@devicename"].Value = "S" + pr.replyPigeonNo;
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "OPEN-TUMB";
                                    updateCmd.ExecuteNonQuery();
                                } // set global user variable = 1 to open the door
                            }
                        }
                        else if (actuation_status == "OPEN-TUMB")
                        {
                            Console.WriteLine("debug1");
                            pr = null;
                            //bytesToSend = new byte[8] { Convert.ToByte(motorAddress), 0x0A, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00 };
                            // GGP : for Trinamic TMCM-6214, just have one card, so the first address is 0x00 
                            bytesToSend = new byte[8] { 0x01, 0x0A, Convert.ToByte(motorAddress), 0x02, 0x00, 0x00, 0x00, 0x00 };
                            bytesToSend = AddByteToArray(bytesToSend, Checksum(bytesToSend));
                            pigeonPort.Write(bytesToSend, 0, bytesToSend.Length);
                            pigeonPort.BaseStream.Flush();

                            //Thread.Sleep(100);
                            /*
                            Thread.Sleep(500);
                            pr = GetReply();
                            */

                            if (WaitReplyUntilTimeout())
                            {
                                //if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 4)
                                // for 6214, the final status is 9
                                if (pr.commandNo == 10 && pr.status == 100 && pr.value4 == 9)
                                {
                                    //updateCmd.Parameters["@devicename"].Value = "S" + pr.replyPigeonNo;
                                    updateCmd.Parameters["@devicename"].Value = "S" + pigeonNo;
                                    updateCmd.Parameters["@status"].Value = "CUP-HERE";
                                    // for infinity loop testing
                                    //updateCmd.Parameters["@status"].Value = "DELIVERY";
                                    updateCmd.ExecuteNonQuery();
                                }
                            }
                        }
                        // 2020-09-03, google tumbler demo for just ust P3 for tumbler : end
                        Thread.Sleep(100);
                    }
                    rdr.Close();

                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.ToString());
                    log.Info(ex.ToString());
                    reconnect = true;
                }
                Thread.Sleep(100);
            } while (true);
            //conn.Close();


            //New couch action -------------------------------------------------------------------------

            /*
            //URI
            Client.BaseAddress = new Uri(BaseCouchDbApiAddress);
            //Cookie and Credentials
            couchDbCredentials = new Credentials()
            {
                name = "admin",
                password = "password"
            };
            string authCookie = GetAuthenticationCookie(couchDbCredentials);
            CookieContainer cc = new CookieContainer();
            cc.Add(new Uri(BaseCouchDbApiAddress), new Cookie(AuthCouchDbCookieKeyName, authCookie));

            List<OrderItem> list = ListAllOrders();
            foreach (OrderItem o in list)
            {
                if (o.status == "COLLECTING")
                {
                    string command = o.location.Replace("S", "H");
                    int pigeonNo = int.Parse(o.location.Substring(1));
                    //Send request to PGH Open
                    pigeonCtrls[pigeonNo].pigeonPort.WriteLine(command);
                    pigeonCtrls[pigeonNo].SetTimerToOpenDoor();

                    //Update Status of PGH closed
                    UpdateOrderStatus(o._id, "OPENED", "newprog");
                    //i++;
                }
            }
            */

            //--------------------------------------------------------------------------------------------

        }

        public static PigeonReply GetReply()
        {
            byte[] cha = new byte[1];
            byte[] buffer = new byte[9];
            int byteGet = 0;
            int bytesGetTotal = 0;
            List<byte> l = new List<byte>();
            try
            {
                do
                {
                    byteGet = pigeonPort.Read(cha, 0, 1);
                    if (byteGet == 1)
                    {
                        //reply = Combine(reply, buffer);
                        l.Add(cha[0]);
                        ++bytesGetTotal;
                    }
                } while (bytesGetTotal < 9);
                buffer = new byte[9] { l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8] };
            }
            catch (TimeoutException e)
            {
                //Console.WriteLine(e.ToString());
                log.Info(e.ToString());
            }
            //Console.WriteLine($"RS485-Reply data: {BitConverter.ToString(buffer)}");
            log.Info($"RS485-Reply data: {BitConverter.ToString(buffer)}");

            PigeonReply pr = new PigeonReply();

            // receive address
            pr.replyAddress = Convert.ToInt32(buffer[0]);
            // module address
            pr.pigeonAddress = Convert.ToInt32(buffer[1]);
            pr.status = Convert.ToInt32(buffer[2]);
            // command number
            pr.commandNo = Convert.ToInt32(buffer[3]);
            pr.value1 = Convert.ToInt32(buffer[4]);
            pr.value2 = Convert.ToInt32(buffer[5]);
            pr.value3 = Convert.ToInt32(buffer[6]);
            pr.value4 = Convert.ToInt32(buffer[7]);
            pr.chksum = Convert.ToInt32(buffer[8]);

            pr.replyPigeonNo = GetPigeonByAddress(pr.pigeonAddress);
            if (pr.replyAddress != 2)
            {
                pr.error = "Reply-Address Not Equal To 2";
            }
            else if (buffer[0] + buffer[1] + buffer[2] + buffer[3] + buffer[4] + buffer[5] + buffer[6] + buffer[7] != buffer[8])
            {
                pr.error = "Check Sum Error ";
            }
            return pr;
        }


        private static void PigeonPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            //Console.WriteLine($"PigeonPort Error : {e.ToString()}");
            log.Info($"PigeonPort Error : {e.ToString()}");
            //throw new NotImplementedException();
        }

        static void sendToClientByPigeonNo()
        {
            int count = 0;
            int pigeonNo = 0;
            string pigeonCmd = "";
            string cmdHead = "";
            string cmdTail = "";
            int sensorState;
            byte[] buffer = new byte[] { 0 };
            do
            {
                pigeonPort.Read(buffer, 0, 1);
                //if (buffer[0] == 10 || buffer[0] == 13 || count > 20)
                if (buffer[0] == ' ')
                {
                    continue;
                }
                if (buffer[0] == ';' || count > 20)
                {
                    break;
                }
                pigeonCmd += System.Text.Encoding.ASCII.GetString(buffer, 0, 1);
                count++;
            } while (true);

            //var sensorCmd = sensorsPort.ReadExisting();
            //Console.WriteLine(pigeonCmd + " Received from pigeons Controller");
            try
            {
                cmdHead = pigeonCmd.Substring(0, 1);
                cmdTail = pigeonCmd.Substring(1);
                if (cmdHead == "X")
                {
                    for (var i = 0; i < clients.Count; i++)
                    {
                        sensorState = int.Parse(cmdTail.Substring(i, 1));
                        clients[i].pigeonCtrl.sensorState = sensorState;
                        //Console.WriteLine($"sensor {i} = {sensorState}");
                    }
                }
                else
                {
                    pigeonNo = int.Parse(cmdTail);
                    // just send to the specific client by pigeon No.
                    clients[pigeonNo - 1].ExecuteCommand(pigeonCmd);
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                log.Info(e.Message);
            }

            //Console.WriteLine("Received : " + Encoding.UTF8.GetString(buffer, offset32, size32));
            //Console.WriteLine("Received : " + sensorCmd);

            //var sensorCmd = sensorsPort.ReadLine();

            //foreach (var client in clients)
            //{
            //    client.ExecuteCommand(sensorCmd);
            //}

        }

        static void PigeonDataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            //Console.WriteLine("Time : {0}", DateTime.Now.TimeOfDay);
            //Task task = new Task(() => sendToAllClients() );
            //var task = new Thread(() => sendToAllClients());

            // multi-task : but cannot used for reading serial data because the data will be broken
            //var task = Task.Factory.StartNew(() => sendToClientByPigeonNo());
            sendToClientByPigeonNo();

            //task.Start();
            //Console.WriteLine("Time : {0}", DateTime.Now.TimeOfDay);
        }

        public static byte Checksum(byte[] data)
        {
            byte sum = 0;
            unchecked // Let overflow occur without exceptions
            {
                foreach (byte b in data)
                {
                    sum += b;
                }
            }
            return sum;
        }

        public static byte[] AddByteToArray(byte[] bArray, byte newByte)
        {
            byte[] newArray = new byte[bArray.Length + 1];
            //bArray.CopyTo(newArray, 1);
            //newArray[0] = newByte;
            bArray.CopyTo(newArray, 0);
            newArray[bArray.Length] = newByte;
            return newArray;
        }
        static public int GetAddressByPigeon(int address)
        {
            return motorAddresses[address - 1];
        }
        static public int GetPigeonByAddress(int motor)
        {
            return motorAddresses.FindIndex(x => x == motor) + 1;
        }

        private static void Pigeon_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //Console.WriteLine("data receiving !");
            pr = GetReply();
        }

        private static bool WaitReplyUntilTimeout()
        {
            // wait around 10 seconds
            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(100);
                if (pr != null)
                {
                    break;
                }
            }
            return (pr != null);
        }


    }

}